# 0.2.1

* Updated Swagger docs to OpenAPI 3.0.
* Changed the HTML theme of Swagger-generated docs.

# 0.2.0

* Added support for keyboard data (key presses and releases).

# 0.1.0

* Initial release.
