from flask import Flask
from flask_cors import CORS

from server.config import Configuration
from server.views import main
from server.views import data
from server.views import session
from server import error


def create_app():
    app = Flask(__name__)
    app.config.from_object(Configuration)
    register_plugins(app)
    register_blueprints(app)
    register_error_handlers(app)

    return app


def register_blueprints(app):
    app.register_blueprint(main.blueprint, url_prefix='/')
    app.register_blueprint(data.blueprint, url_prefix='/data')
    app.register_blueprint(session.blueprint, url_prefix='/session')


def register_plugins(app):
    CORS(app)


def register_error_handlers(app):
    app.register_blueprint(error.blueprint)
