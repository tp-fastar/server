from flask_env import MetaFlaskEnv


class Configuration(metaclass=MetaFlaskEnv):
    DEBUG = True
    MONGO_URI = 'mongodb://localhost/behametrics'
    LOGGER_HEADER_NAME = 'Logger'
    LOGGER_HEADER_PREFIX = 'behametrics/'
