from functools import wraps
import json

from flask import request
from marshmallow import EXCLUDE
from webargs import dict2schema

from server.config import Configuration


class JsonQueryParser:
    """
    Class that deserializes a stringified JSON object into a MongoDB query.
    """

    def __init__(self, query_filter=None):
        self.schema = None
        if query_filter is not None:
            self.schema = dict2schema(query_filter)()

    def __call__(self, data, ctx=None):
        parsed = json.loads(data)
        if self.schema is not None:
            return self.schema.load(parsed, unknown=EXCLUDE)
        else:
            return parsed


def only_logger(fun):
    """
    Decorator that allows only requests with the correct value for the header
    named `Configuration.LOGGER_HEADER_NAME`.
    """
    @wraps(fun)
    def wrap(*args, **kwargs):
        header = request.headers.get(Configuration.LOGGER_HEADER_NAME)
        if not _is_header_valid(header):
            return '', 403

        return fun(*args, **kwargs)

    return wrap


def _is_header_valid(header):
    return (header is not None
            and header.lower().startswith(Configuration.LOGGER_HEADER_PREFIX))


def normalize_input_name(input_name):
    """Return the specified input name in lowercase."""
    return input_name.lower()
