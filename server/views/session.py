from flask import Blueprint
from flask import jsonify
from flask import request
from webargs import fields
from webargs.flaskparser import use_kwargs

from server.dao import get_sessions_dao
from server.util import only_logger

blueprint = Blueprint('session', __name__)


@blueprint.route('', methods=['POST'])
@only_logger
def create_session():
    session = get_sessions_dao().save(request.get_json())
    return jsonify({'id': session['_id']}), 201


@blueprint.route('/<string:session_id>', methods=['GET'])
def fetch_session_data(session_id):
    data = get_sessions_dao().get_one({
        '_id': session_id
    })
    return jsonify(data)


@blueprint.route('/for-user', methods=['GET'])
@use_kwargs({
    'username': fields.Str(required=True)
})
def fetch_sessions_ids_for_user(username):
    data = get_sessions_dao().get(
        query={
            'username': {
                '$eq': username
            }
        })

    ids = [document['_id'] for document in data]
    return jsonify(ids)


@blueprint.route('/usernames-list', methods=['GET'])
def fetch_usernames():
    data = get_sessions_dao().get(
        query={
            'username': {
                '$exists': True
            }
        })

    return jsonify([document['username'] for document in data])
