import pytest
from unittest.mock import MagicMock

from server import services
from server.services import data as service

from tests.util import patch_with_mock
from tests.util import stub_generator
from tests.util import stub_based_on_arg_value


def test_fetch_for_query(monkeypatch):
    events_dao_mock = patch_with_mock(
        monkeypatch, services.data, 'get_events_dao')

    query = {'input': 'wheel', 'session_id': '5c70579af2307d000b7eaf0c'}

    service.fetch_for_query(query=query)

    events_dao_mock.get.assert_called_once_with(query=query)


def test_save_data(monkeypatch):
    events_dao_mock = patch_with_mock(
        monkeypatch, services.data, 'get_events_dao')

    data = {'input': 'wheel', 'timestamp': 456, 'content': {'delta_x': 1}}

    service.save_data(data)

    events_dao_mock.save.assert_called_once_with(data)


@pytest.mark.parametrize('columns,query_output,expected_lines', [
    ((['input', 'timestamp'], ['delta_x', 'delta_y']),
     [
         {
             'input': 'wheel',
             'timestamp': 456,
             'content': {
                 'delta_x': 5,
                 'delta_y': 10,
             },
         },
         {
             'input': 'wheel',
             'timestamp': 456,
             'content': {
                 'delta_x': 5,
             },
         },
         {
             'input': 'wheel',
             'content': {
                 'delta_x': 5,
             },
         },
         {
             'input': 'wheel',
         },
         {}
     ],
     ['input,timestamp,delta_x,delta_y',
      'wheel,456,5,10',
      'wheel,456,5,',
      'wheel,,5,',
      'wheel,,,',
      ',,,']),
    ((['input', 'timestamp'], ['delta_x', 'delta_y']),
     [
         {
             'input': 'wheel',
             'content': {
                 'delta_x': 5,
                 'z': 2,
             },
         },
         {
             'input': 'wheel',
             'timestamp': None,
             'content': {
                 'delta_x': 5,
             },
         },
         {
             'input': 'wheel',
             'timestamp': None,
             'content': {
                 'delta_x': '120,150',
             },
         },
         {
             'input': 'wheel',
             'timestamp': None,
             'content': {
                 'delta_x': {'first': 120, 'second': 150},
             },
         },
     ],
     ['input,timestamp,delta_x,delta_y',
      'wheel,,5,',
      'wheel,,5,',
      'wheel,,120;150,',
      'wheel,,{\'first\': 120; \'second\': 150},']),
])
def test_create_csv(columns, query_output, expected_lines, monkeypatch):
    events_dao_mock = patch_with_mock(
        monkeypatch, services.data, 'get_events_dao')
    
    query = {'sample': 'query'}
    cursor = stub_generator(query_output)

    events_dao_mock.get_cursor.side_effect = stub_based_on_arg_value(
        arg_check=lambda x: x == query,
        return_value=cursor
    )

    csv_gen = service.create_csv(query, columns)

    expected_csv = '\n'.join(expected_lines) + '\n'

    assert ''.join(csv_gen) == expected_csv


@pytest.mark.parametrize('query,input_name,expected_query', [
    ({'sample': 'query'},
     'unknown',
     {'sample': 'query',
      'input': 'unknown',
      'session_id': {'$exists': True},
      'timestamp': {'$exists': True}
      }),
    ({'session_id': {'$exists': False}},
     'unknown',
     {'input': 'unknown',
      'session_id': {'$exists': True},
      'timestamp': {'$exists': True}
      }),
])
def test_get_query_for_input(query, input_name, expected_query):
    assert service.get_query_for_input(query, input_name) == expected_query


@pytest.mark.parametrize('input_name,aggregate_result,expected_columns', [
    ('wheel',
     {
         'commonFieldNames': ['input', 'session_id', 'timestamp'],
         'contentFieldNames': ['button', 'delta_x', 'delta_y'],
     },
     (['input', 'session_id', 'timestamp'],
      ['button', 'delta_x', 'delta_y'])),
    ('wheel',
     {
         'commonFieldNames': ['input', 'session_id', 'timestamp', 'd', 'c'],
         'contentFieldNames': ['button', 'z', 'a', 'delta_x', 'delta_y'],
     },
     (['input', 'session_id', 'timestamp', 'c', 'd'],
      ['button', 'delta_x', 'delta_y', 'a', 'z'])),
    ('unknown',
     {
         'commonFieldNames': ['input', 'timestamp', 'session_id'],
         'contentFieldNames': ['y', 'x'],
     },
     (['input', 'session_id', 'timestamp'], ['x', 'y'])),
    ('unknown',
     {
         'commonFieldNames': ['x'],
         'contentFieldNames': ['x'],
     },
     (['x'], ['x'])),
    ('unknown',
     {
         'commonFieldNames': ['content', '_id', 'timestamp'],
         'contentFieldNames': ['content', '_id'],
     },
     (['timestamp'], ['content'])),
    ('unknown',
     {
         'commonFieldNames': [],
         'contentFieldNames': [],
     },
     ([], [])),
    (None,
     {
         'commonFieldNames': [],
         'contentFieldNames': [],
     },
     None)
])
def test_get_columns_for_input(
        input_name, aggregate_result, expected_columns, monkeypatch):
    events_dao_mock = patch_with_mock(
        monkeypatch, services.data, 'get_events_dao')
    
    cursor_stub = MagicMock()
    cursor_stub.next = lambda: aggregate_result
    
    events_dao_mock.aggregate.return_value = cursor_stub
    
    assert service.get_columns_for_input(input_name) == expected_columns


def test_get_columns_for_input_for_no_matching_data(monkeypatch):
    events_dao_mock = patch_with_mock(
        monkeypatch, services.data, 'get_events_dao')
    
    cursor_stub = MagicMock()
    cursor_stub.next.side_effect = StopIteration()
    
    events_dao_mock.aggregate.return_value = cursor_stub
    
    assert service.get_columns_for_input('unknown') is None


@pytest.mark.parametrize('columns_common,columns_content,expected_columns', [
    (['input', 'timestamp'],
     ['x', 'y'],
     ['input', 'timestamp', 'x', 'y']),
    (['input', 'timestamp'],
     [],
     ['input', 'timestamp']),
    ([],
     [],
     []),
    (['input', 'x'],
     ['x', 'y'],
     ['input', 'x', 'content_x', 'y']),
    (['input', 'x', 'content_x'],
     ['x', 'y'],
     ['input', 'x', 'content_x', 'content_x_2', 'y']),
    (['input', 'x', 'content_x', 'content_x_2'],
     ['x', 'y'],
     ['input', 'x', 'content_x', 'content_x_2', 'content_x_3', 'y']),
    (['input', 'x', 'y'],
     ['x', 'y'],
     ['input', 'x', 'y', 'content_x', 'content_y']),
])
def test_get_unique_column_names(
        columns_common, columns_content, expected_columns):
    assert (
        service.get_unique_column_names(columns_common, columns_content)
        == expected_columns)


@pytest.mark.parametrize('input_name,expected_query', [
    ('wheel',
     {
         'session_id': {'$exists': True},
         'timestamp': {'$exists': True},
         'content.button': {'$exists': True},
         'content.delta_x': {'$exists': True},
         'content.delta_y': {'$exists': True},
         'content.cursor_x': {'$exists': True},
         'content.cursor_y': {'$exists': True},
     }),
    ('unknown',
     {
         'session_id': {'$exists': True},
         'timestamp': {'$exists': True},
     }),
])
def test_get_match_query(input_name, expected_query):
    expected_query['input'] = input_name
    
    assert service.get_match_query(input_name) == expected_query
