from bson import ObjectId

import pytest
from unittest.mock import Mock

from server.dao import Dao
from server.dao import get_events_dao
from server.dao import get_sessions_dao


@pytest.fixture()
def collection():
    return Mock()


@pytest.fixture()
def mongo_client(collection):
    client = Mock()

    db_mock = Mock()
    db_mock.get_collection.return_value = collection
    client.get_database.return_value = db_mock

    return client


@pytest.fixture()
def dao(mongo_client):
    class TestDao(Dao):
        def __init__(self):
            super().__init__(collection_name='test', client=mongo_client)

    return TestDao()


def test_that_daos_have_correct_collection_name():
    assert get_sessions_dao()._collection_name == 'sessions'
    assert get_events_dao()._collection_name == 'events'


def test_one_item_is_saved(dao, collection):
    data = {'_id': '123'}

    dao.save(data)

    collection.insert_one.assert_called_once_with(data)


def test_multiple_items_are_saved(dao, collection):
    data = [{'_id': i} for i in range(15)]

    dao.save(data)

    collection.insert_many.assert_called_once_with(data)


def test_every_saved_item_has_string_id(dao, collection):
    data = {'_id': ObjectId('5c70579af2307d000b7eaf0c')}
    data_list = [data, data]

    collection.insert_one.return_value = data
    collection.insert_many.return_value = data_list

    saved_item = dao.save(data)
    saved_list = dao.save(data_list)

    assert isinstance(saved_item['_id'], str)
    assert all([isinstance(i['_id'], str) for i in saved_list])


def test_all_items_are_retrieved(dao, collection):
    data = [{'_id': str(i)} for i in range(15)]

    collection.find.return_value = data

    assert data == dao.get()


def test_get_correctly_uses_query(dao, collection):
    query = {'sample': 'query'}
    result = [
        {'_id': '123', 'sample': 'query'}, {'_id': '234', 'sample': 'query'}]

    collection.find.return_value = result

    dao.get(query)

    collection.find.assert_called_once_with(query)


def test_item_is_retrieved(dao, collection):
    data = {'_id': '123'}

    collection.find_one.return_value = data

    assert data == dao.get_one()


def test_item_is_None(dao, collection):
    collection.find_one.return_value = None

    assert dao.get_one() is None


def test_get_one_correctly_uses_query(dao, collection):
    query = {'sample': 'query'}
    result = {'_id': '123', 'sample': 'query'}

    collection.find_one.return_value = result

    dao.get_one(query)

    collection.find_one.assert_called_once_with(query)


def test_update_one_correctly_uses_args(dao, collection):
    update = {'$inc': 1}

    dao.update_one(update)

    collection.update_one.assert_called_once_with({}, update)


def test_update_one_correctly_uses_kwargs(dao, collection):
    criteria = {'abc': 123}
    update = {'$inc': 1}
    kwargs = {'upsert': True}

    dao.update_one(update, criteria=criteria, **kwargs)

    collection.update_one.assert_called_once_with(criteria, update, **kwargs)


def test_get_cursor_default_query(dao, collection):
    dao.get_cursor()
    collection.find.assert_called_once_with({})
